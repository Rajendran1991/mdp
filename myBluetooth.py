#====================================================================#
#==========================  MDP Grp 14   ===========================#
#=======================  mybluetooth.py  ===========================#
#====================================================================#
from bluetooth import *
import threading
import time


class blueToothListener (threading.Thread):

    client_sock = 0
    
    def __init__(self, threadID, name):
            threading.Thread.__init__(self)
            self.threadID = threadID
            self.name = name
            #self.counter = counter
    def run(self):
##                print "Starting "  + self.name
##                counter = 5
##                while counter:
##                        time.sleep(1)
##                        print "counter: %s" % counter
##                        counter -= 1
        while True:
                server_sock=BluetoothSocket( RFCOMM )
                server_sock.bind(("",4))
                server_sock.listen(1)
        
                port = server_sock.getsockname()[1]
        
                uuid = "94f39d29-7d6d-437d-973b-fba39e49d4ee"
        
                advertise_service( server_sock, "SampleServer",
                           service_id = uuid,
                           service_classes = [ uuid, SERIAL_PORT_CLASS ],
                           profiles = [ SERIAL_PORT_PROFILE ], 
        #                   protocols = [ OBEX_UUID ] 
                            )
                           
                print("Waiting for connection on RFCOMM channel %d" % port)

                global client_sock
                client_sock, client_info = server_sock.accept()
                print("Accepted connection from ", client_info)
        
                try:
                    while True:
                        data = client_sock.recv(1024)
                        if len(data) == 0: break
                        if data =="end":
                            print("stopping the bluetooth listener")
                            client_sock.close()
                            server_sock.close()
                            return
                            
                        myReceive(data)

                except IOError:
                    pass
        
                print("disconnected")
        
                client_sock.close()
                server_sock.close()

        #end of while loop

    #end of run method
    
def myReceive(data):
    #do something
    if data == "w":     #move up
        mySend("received movement: [%s]" % data)
    elif data == "s":   #move down
        mySend("received movement: [%s]" % data)
    elif data == "a":   #move left
        mySend("received movement: [%s]" % data)
    elif data == "d":   #move right
        mySend("received movement: [%s]" % data)
    else:
        mySend("received the rest loh [%s]" % data)


def mySend(msg):
    try:
        client_sock.send(msg)
    except:
        print("error sending message %s" % msg)




        
